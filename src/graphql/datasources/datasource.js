const { RESTDataSource } = require('apollo-datasource-rest')

exports.spotifyWebAPI = class extends RESTDataSource {
  constructor() {
    super()
    this.baseURL = 'https://api.spotify.com/v1/'
  }

  willSendRequest(request) {
    request.headers.set('Authorization', this.context.token)
  }

  // ME

  getMe() {
    return this.get('me')
  }

  getMyPlaylists(){
    return this.get('me/playlists')
  }

  async getMyAlbums() {
    const results = await this.get('me/albums')
    return results.items.map(res => res.album)
  }

  async getMyTracks() {
    const results = await this.get('me/tracks')
    return results.items.map(res => res.track)
  }

  // ARTIST

  async getArtistById(id) {
    let [artistInfo, albums, topTracks] = await Promise.all([
      this.get(`artists/${id}`),
      this.get(`artists/${id}/albums?include_groups=album&country=from_token`),
      this.get(`artists/${id}/top-tracks?country=from_token`)
    ])

    //THIS COULD USE SOME PROPER MANGLING BUT LET'S LEAVE IT LIKE THIS FOR NOW
    return {
      ...artistInfo,
      ...albums,
      ...topTracks
    }
  }

  //ALBUM

  async getAlbumById(id) {
    const albumInfo = await this.get(`albums/${id}`)
    const tracks = await this.get(`albums/${id}/tracks`)

    return {
      ...albumInfo,
      ...tracks
    }
  }

  getNowPlaying() {
    return this.get('me/player')
  }

  //Search
  async search(query) {
    const result =  await this.get(`search?q=${query}&market=from_token&type=artist,track`)

    return {
      artists: result.artists.items,
      tracks: result.tracks.items
    }
  }

  // Playlist
  async getPlaylistById(id) {
    const result = await this.get(`playlists/${id}`)
    const tracks = result.tracks.items.map(item => item.track)
    return {
      ...result,
      tracks
    }
  }
}

exports.sonosApi = class extends RESTDataSource {
  constructor() {
    super()
    this.baseURL = 'http://localhost:5005/'
  }

  getZones(){
    return this.get('zones')
  }
  
  async play(zoneName){
    const response = await this.get(`${zoneName}/play`)
    return response.status

  }

}

