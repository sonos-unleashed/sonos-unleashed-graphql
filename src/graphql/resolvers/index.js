const resolvers = {
  Query: {
    me: (_source, _args, { dataSources }) => dataSources.spotify.getMe(),
    artist: (_source, _args, { dataSources }) => dataSources.spotify.getArtistById(_args.id),
    album: (_source, _args, { dataSources }) => dataSources.spotify.getAlbumById(_args.id),
    player: (_source, _args, { dataSources }) => dataSources.spotify.getNowPlaying(),
    search: (_source, _args, { dataSources }) => dataSources.spotify.search(_args.query),
    playlist: (_source, _args, { dataSources }) => dataSources.spotify.getPlaylistById(_args.id),
    zones: (_source, _args, { dataSources }) => dataSources.sonos.getZones()
  },
  Mutation: {
    play: (_source, _args, { dataSources }) => dataSources.sonos.play(_args.zoneName)
  },
  User: {
    username: ({ display_name }) => display_name,
    email: ({ email }) => email,
    playlists: (_source, _args, { dataSources }) => dataSources.spotify.getMyPlaylists().then(res => res.items),
    albums: (_source, _args, { dataSources }) => dataSources.spotify.getMyAlbums(),
    tracks: (_source, _args, { dataSources }) => dataSources.spotify.getMyTracks()
  },
  Artist: {
    topTracks: ({ tracks }) => (tracks),
    albums: ({ items }) => (items)
  },
  Player: {
    trackName: ({ item }) => (item.name),
    artistName: ({ item }) => (item.artists[0].name)
  },
  Album: {
    tracks: ({ tracks }) => (tracks.items)
  }

}

module.exports = resolvers