const { gql } = require('apollo-server-express')

const typeDefs = gql`
  type Query {
    me: User
    player: Player
    artist(id: ID!): Artist
    album(id: ID!): Album
    search(query: String!): Result
    playlist(id: ID!): Playlist
    zones: [Zone]
  }

  type Mutation {
    play(zoneName: String!): String!
  }

  type Result {
    artists: [Artist]
    tracks: [Track]
  }

  type User {
    username: String
    display_name: String
    email: String
    playlists: [Playlist]
    albums: [Album]
    tracks: [Track]
  }

  type Player {
    trackName: String
    artistName: String
  }

  type Playlist {
    name: String
    id: ID
    tracks: [Track]
    owner: Owner
    images: [Image]
  }

  type Album {
    id: ID
    album_type: String
    artists: [Artist]
    images: [Image]
    name: String
    tracks: [Track]
  }

  type Artist {
    id: ID
    name: String
    images: [Image]
    topTracks: [Track]
    albums: [Album]
  }

  type Image {
    height: String
    width: String
    url: String
  }

  type Track {
    id: ID
    name: String
    album: Album
    artists: [Artist]
  }

  type Owner {
    id: ID
    display_name: String
  }
  
  type Zone {
    uuid: String
    members: [Member]
    coordinator: String
    state: State
  }

  type Member {
    uuid: String
    roomName: String
    coordinator: String
    state: State

  }
  type State {
    volume: Int
    mute: Boolean
    currentTrack: SonosTrack
    playbackState: String
  }
  type SonosTrack {
    artist: String
    title: String
    album: String
    duration: Int
    type: String
    uri: String
  }
`

module.exports = typeDefs