const { ApolloServer } = require('apollo-server-express')
const express = require('express')
const cookieParser = require('cookie-parser')
const cors = require('cors')

const app = express()
app.use(cookieParser())
app.use(cors())

const typeDefs = require('./src/graphql/schema/index')
const resolvers = require('./src/graphql/resolvers/index')
const { spotifyWebAPI, sonosApi } = require('./src/graphql/datasources/datasource')

const server = new ApolloServer({
  typeDefs,
  resolvers,
  context: ({req}) => {
    const token = req.headers.authorization
    return {token}
  },
  dataSources: () => ({
    spotify: new spotifyWebAPI(),
    sonos: new sonosApi()
  })
})

server.applyMiddleware({ app, path: '/graphql' })

const spotify = require('./src/routes/auth/spotify.js')
app.use('/spotify', spotify)

app.listen({ port: 9000 }, () => {
  console.log('Apollo Server on http://localhost:9000/graphql')
})


